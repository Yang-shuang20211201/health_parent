package com.czjk.service.impl;

import com.czjk.entity.PageResult;
import com.czjk.mapper.CheckGroupAndCheckItemMapper;
import com.czjk.mapper.CheckGroupMapper;
import com.czjk.mapper.CheckItemMapper;
import com.czjk.mapper.SetMealAndCheckgroupMapper;
import com.czjk.pojo.CheckGroup;
import com.czjk.pojo.CheckItem;
import com.czjk.service.CheckGroupService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 *
 */
@Service
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupMapper checkGroupMapper;
    @Autowired
    private CheckGroupAndCheckItemMapper checkGroupAndCheckItemMapper;
    @Autowired
    private CheckItemMapper checkItemMapper;
    @Autowired
    private SetMealAndCheckgroupMapper setMealAndCheckgroupMapper;

    @Override
    public PageResult findByPage(Integer currentPage, Integer pageSize, String queryString) {
        //1 设置分页参数
        PageHelper.startPage(currentPage,pageSize);
        //2 带条件分页查询
        Page page=(Page)checkGroupMapper.findAll(queryString);
        //3 封装分页结果并返回
        return new PageResult(page.getTotal(),page.getResult());
    }

    //根据id查询检查项
    @Transactional
    @Override
    public List<CheckItem> findCheckItemsById(Integer checkGroupId) {
        return checkItemMapper.findCheckItemsByCheckGroupId(checkGroupId);
    }

    //获取检查组
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupMapper.findById(id);
    }

    //添加检查组
    @Transactional
    @Override
    public void addCheckGroup(CheckGroup checkGroup, Integer[] ids) {
        //插入检查组数据
        checkGroupMapper.add(checkGroup);
        //向关系表中添加数据(操作中间表t_checkgroup_checkitem)，思考：此处需要检查组id，如何拿到？
        if(ids!=null && ids.length>0){
            for (Integer checkItemId : ids) {
                checkGroupAndCheckItemMapper.addCheckGroupAndCheckItem(checkGroup.getId(),checkItemId);
            }
        }
    }

    //更新检查组
    @Transactional
    @Override
    public void updateCheckGroup(CheckGroup checkGroup, Integer[] ids) {
        //更新检查组信息
        checkGroupMapper.update(checkGroup);
        //更新检查组和检查项的关联关系(多对多)(先删除关联关系，再添加关联关系)
        if(ids!=null && ids.length>0){
            //先删除关联关系(操作中间表t_checkgroup_checkitem)
            checkGroupAndCheckItemMapper.deleteCheckGroupAndCheckItem(checkGroup.getId());
            //再添加关联关系(操作中间表t_checkgroup_checkitem)
            for (Integer checkItemId : ids) {
                checkGroupAndCheckItemMapper.addCheckGroupAndCheckItem(checkGroup.getId(),checkItemId);
            }
        }
        //其实此处还需要更新检查组和套餐的关联关系(多对多)，此处暂时不做！！！
    }

    //删除检查组
    @Transactional
    @Override
    public void deleteCheckGroup(Integer id) {
        //先删除套餐和检查组的关联关系(操作中间表t_setmeal_checkgroup)，此处暂时不做！！！
        //先删除检查组和检查项的关联关系(操作中间表t_checkgroup_checkitem)
        checkGroupAndCheckItemMapper.deleteCheckGroupAndCheckItem(id);
        //再删除检查组信息
        checkGroupMapper.delete(id);
    }

    //查找所有
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupMapper.findAll(null);
    }
}
