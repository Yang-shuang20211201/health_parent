package com.czjk.service.impl;


import com.czjk.mapper.OrderSettingMapper;
import com.czjk.pojo.OrderSetting;
import com.czjk.service.OrderSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Service
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingMapper orderSettingMapper;
	/**
     * 将预约设置的集合存入数据库中
     * @param excels excel中每行的值存到一个数组，所有行存到一个集合返回
     * 思考：如何在不查询数据库的情况下，判断该日期的预约设置是否存在？
     */
    @Override
    public void importSettings(List<String[]> excels) {
        //遍历List<String[]>集合，将每个String[]中的数据封装成OrderSetting对象，设置或者更新可预人数
        excels.forEach(arr->{
            //将日期转换成LocalDate类型
            LocalDate orderDate = LocalDate.parse(arr[0], DateTimeFormatter.ofPattern("yyyy/MM/dd"));
            //创建OrderSetting对象并设置参数
            OrderSetting orderSetting = new OrderSetting(orderDate, Integer.parseInt(arr[1]));
            //如果数据库中已经存在该日期的预约设置 则更新
            int count=orderSettingMapper.update(orderSetting);
            //如果数据库中不存在该日期的预约设置 则插入
            if(count==0){
                orderSettingMapper.add(orderSetting);
            }
        });
    }

    /**
     * 查询所有预约
     * @param currentYear 当前年份
     * @param currentMonth 当前月份
     * @return
     */
    @Override
    public List<Map> findAll(String currentYear, String currentMonth) {
        return orderSettingMapper.findAll(currentYear,currentMonth);
    }

    /**
     * 设置或者更新可预约人数
     * @param orderSetting
     */
    @Override
    public void setting(OrderSetting orderSetting) {
        //如果数据库中已经存在该日期的预约设置 则更新
        int count=orderSettingMapper.update(orderSetting);
        //如果数据库中不存在该日期的预约设置 则插入
        if(count==0){
            orderSettingMapper.add(orderSetting);
        }
    }
}
