package com.czjk.service.impl;

import com.czjk.entity.PageResult;
import com.czjk.mapper.CheckGroupAndCheckItemMapper;
import com.czjk.mapper.CheckItemMapper;
import com.czjk.pojo.CheckGroupAndCheckItem;
import com.czjk.pojo.CheckItem;
import com.czjk.service.CheckItemService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 体检检查项管理
 */
@Service
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemMapper checkItemMapper;

    @Override
    public PageResult findByPage(Integer currentPage, Integer pageSize, String queryString) {
        //1 设置分页参数
        PageHelper.startPage(currentPage,pageSize);
        //2 带条件分页查询
        Page page=(Page)checkItemMapper.findAll(queryString);
        //3 封装分页结果并返回
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 添加检查项
     */
    @Override
    public void add(CheckItem checkItem) {
        checkItemMapper.add(checkItem);
    }

    /**
     * 根据id查询对象
     */
    @Override
    public CheckItem findById(Integer id) {
        return checkItemMapper.findById(id);
    }

    /**
     * 修改checkItem
     */
    @Override
    public void update(CheckItem checkItem) {
        checkItemMapper.update(checkItem);
    }

    /**
     * 删除
     */
    @Override
    public void deleteCheckItem(Integer id) {
        checkItemMapper.deleteCheckItem(id);
    }

    //查询所有
    @Override
    public List<CheckItem> findAll() {
        return checkItemMapper.findAll(null);
    }
}
