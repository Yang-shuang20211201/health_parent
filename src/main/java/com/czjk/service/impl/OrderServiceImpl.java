package com.czjk.service.impl;

import com.czjk.entity.Result;
import com.czjk.mapper.MemberMapper;
import com.czjk.mapper.OrderMapper;
import com.czjk.mapper.OrderSettingMapper;
import com.czjk.mapper.SetMealMapper;
import com.czjk.pojo.Member;
import com.czjk.pojo.Order;
import com.czjk.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 *
 */
@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderSettingMapper orderSettingMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private SetMealMapper setMealMapper;

    @Transactional
    @Override
    public Result postOrder(Order order, Member member) {
        //从数据库中获取member的id
        //将表单数据插入表单表中
        //预约人数+1
        return null;
    }

    //判断该用户是否在同一天预定过该套餐
    @Transactional
    @Override
    public Result hasOrderedSetMeal(Member member, String orderDate, Integer setmealId) {
        //先根据姓名查找id
        //如果不存在此用户 说明尚未在该日期订购过套餐
        //注册该用户
        //如果用户未在当天订购过该套餐
        //如果用户订购该套餐
        return null;
    }


    //根据id获取订单
    @Override
    public Order findById(Integer id) {
        return null;

    }

    //根据套餐查找数量 封装成map集合
    @Transactional
    @Override
    public Map findBySetmeal() {
        return null;
    }
}
