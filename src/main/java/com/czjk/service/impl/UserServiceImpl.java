package com.czjk.service.impl;


import com.czjk.mapper.*;
import com.czjk.pojo.Permission;
import com.czjk.pojo.Role;
import com.czjk.pojo.User;
import com.czjk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserAndRoleMapper userAndRoleMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleAndPermissionMapper roleAndPermissionMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    //根据用户名查找用户 包括用户对应的角色集合 以及角色对应的权限集合
    @Override
    public User findByName(String s) {
        return null;
    }


    //根据user的id查询对应的role的id集合
    public List<Integer> findRoleIdByUserId(Integer userId) {
        return null;

    }

    //根据role的id集合查找role的集合
    public List<Role> findRoleByRoleId(List<Integer> roleIds) {
        return null;

    }

    //根据role查找对应的权限id的集合
    public List<Integer> findPermissionIdByRole(Role role) {
        return null;

    }

    //根据permission的id查找权限的集合
    public List<Permission> findPermissionByPermissionId(List<Integer> permissionIds) {
        return null;
    }
}
