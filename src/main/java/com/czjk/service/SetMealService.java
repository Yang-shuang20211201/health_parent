package com.czjk.service;

import com.czjk.entity.PageResult;
import com.czjk.pojo.CheckGroup;
import com.czjk.pojo.Setmeal;

import java.util.List;

/**
 *
 */
public interface SetMealService {

    //分页查找套餐
    PageResult findByPage(Integer currentPage, Integer pageSize, String queryString);

    //添加套餐
    void add(Setmeal setmeal, Integer[] ids);

    //根据id查询
    Setmeal findById(Integer id);

    //根据id查询检查组
    List<CheckGroup> findCheckGroupById(Integer id);

    //更新套餐
    void update(Setmeal setmeal, Integer[] ids);

    //删除套餐
    void delete(Integer id);
}
