package com.czjk.service;

import com.czjk.entity.Result;
import com.czjk.pojo.OrderSetting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface OrderSettingService {

    /**
     * 将预约设置的集合存入数据库中
     * @param excels excel中每行的值存到一个数组，所有行存到一个集合返回
     * 思考：如何在不查询数据库的情况下，判断该日期的预约设置是否存在？
     */
    void importSettings(List<String[]> excels);

    /**
     * 查询当前月份的预约数
     * @param currentYear 当前年份
     * @param currentMonth 当前月份
     * @return
     */
    List<Map> findAll(String currentYear, String currentMonth);

    /**
     * 设置或者更新可预约人数
     * @param orderSetting
     */
    void setting(OrderSetting orderSetting);
}
