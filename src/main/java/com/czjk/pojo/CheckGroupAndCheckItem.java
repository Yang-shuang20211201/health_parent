package com.czjk.pojo;

import lombok.Data;

@Data
public class CheckGroupAndCheckItem {
    private Integer checkGroupId;
    private Integer checkItemId;
}
