package com.czjk.pojo;

import lombok.Data;

@Data
public class SetMealAndCheckgroup {
    private Integer setMealId;
    private Integer checkGroupId;
}
