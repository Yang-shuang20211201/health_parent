package com.czjk.pojo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 权限
 */
@Data
public class Permission implements Serializable {

    private Integer id;
    private String name; // 权限名称
    private String keyword; // 权限关键字，用于权限控制
    private String description; // 描述
}
