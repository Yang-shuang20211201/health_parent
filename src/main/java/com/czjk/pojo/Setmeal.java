package com.czjk.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 体检套餐
 */
@Data
public class Setmeal implements Serializable {

    private Integer id;
    private String name;
    private String code;
    private String helpCode;
    private String sex;//套餐适用性别：0不限 1男 2女
    private String age;//套餐适用年龄
    private Float price;//套餐价格
    private String remark;
    private String attention;
    private String img;//套餐对应图片存储路径
}
