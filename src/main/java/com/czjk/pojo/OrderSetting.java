package com.czjk.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * 预约设置
 */
@Data
@NoArgsConstructor
public class OrderSetting implements Serializable {
    private Integer id ;
    private LocalDate orderDate;//预约设置日期
    private Integer number;//可预约人数
    private Integer reservations ;//已预约人数

    public OrderSetting(LocalDate orderDate, Integer number) {
        this.orderDate = orderDate;
        this.number = number;
    }
}
