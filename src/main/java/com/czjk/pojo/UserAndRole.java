package com.czjk.pojo;

import lombok.Data;

@Data
public class UserAndRole {

    private Integer userId;
    private Integer roleId;
}
