package com.czjk.pojo;

import lombok.Data;

@Data
public class RoleAndPermission {

    private Integer roleId;
    private Integer permissionId;
}
