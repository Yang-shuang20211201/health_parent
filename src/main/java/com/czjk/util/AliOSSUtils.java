package com.czjk.util;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 阿里云 OSS 工具类
 * 使用说明：
 *      1、需要自己去aliyun oss对象存储服务中创建bucket桶，创建accessKeyId和accessKeySecret
 *      2、修改application.properties中的配置信息
 *              aliyun.oss.endpoint=https://xxx
 *              aliyun.oss.access-key-id=xxx
 *              aliyun.oss.access-key-secret=xxx
 *              aliyun.oss.bucket-name=xxx
 */
@Component
@Data
public class AliOSSUtils {
    @Autowired
    private AliOSSProperties aliOSSProperties;
    /**
     * 实现上传图片到OSS
     */
    public String upload(MultipartFile multipartFile) throws IOException {
        //获取配置信息
        String endpoint = aliOSSProperties.getEndpoint();
        String accessKeyId = aliOSSProperties.getAccessKeyId();
        String accessKeySecret = aliOSSProperties.getAccessKeySecret();
        String bucketName = aliOSSProperties.getBucketName();

        //1 获取上传的文件的输入流
        InputStream inputStream = multipartFile.getInputStream();

        //2 获取原始文件名，使用UUID生成随机字符串，组装唯一文件名
        String originalFilename = multipartFile.getOriginalFilename();
        String filename = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));

        //3 上传文件到 OSS
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, filename, inputStream);

        //4 获取文件访问路径
        String url = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + "/" + filename;
        //5 关闭ossClient客户端，返回文件在OSS存储空间的地址
        ossClient.shutdown();
        return url;
    }
	
	/**
     * 实现删除OSS存储空间的图片
     */
    public void delete(String filename){
        String endpoint =aliOSSProperties.getEndpoint();
        String accessKeyId = aliOSSProperties.getAccessKeyId();
        String accessKeySecret = aliOSSProperties.getAccessKeySecret();
        String bucketName =aliOSSProperties.getBucketName();

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 删除文件。
        ossClient.deleteObject(bucketName, filename);
        // 关闭ossClient客户端
        ossClient.shutdown();
    }

}
