package com.czjk.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

public class POIUtils {
    private final static String xls = "xls";
    private final static String xlsx = "xlsx";
    private final static String DATE_FORMAT = "yyyy/MM/dd";
    /**
     * 读入excel文件，解析后返回
     * @param file
     * @throws IOException
     */
    public static List<String[]> readExcel(MultipartFile file) throws IOException {
        //检查文件
        checkFile(file);
        //获得Workbook工作薄对象
        Workbook workbook = getWorkBook(file);
        //创建返回对象，把excel中每行的值存到一个数组，所有行存到一个集合返回
        List<String[]> list = new ArrayList<String[]>();

        if(workbook != null){
            for(int sheetNum = 0;sheetNum < workbook.getNumberOfSheets();sheetNum++){
                //获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if(sheet == null){
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum  = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                //循环除了第一行的所有行
                for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if(row == null){
                        continue;
                    }
                    //获得当前行的开始列
                    int firstCellNum = row.getFirstCellNum();
                    //获得当前行的列数
                    int lastCellNum = row.getPhysicalNumberOfCells();
                    String[] cells = new String[row.getPhysicalNumberOfCells()];
                    for(int cellNum = firstCellNum; cellNum < lastCellNum;cellNum++){
                        Cell cell = row.getCell(cellNum);
                        cells[cellNum] = getCellValue(cell);
                    }
                    list.add(cells);
                }
            }
            workbook.close();
        }
        return list;
    }
    //校验文件是否合法
    private static void checkFile(MultipartFile file) throws IOException{
        //判断文件是否存在
        if(null == file){
            throw new FileNotFoundException("文件不存在！");
        }
        //获得文件名
        String fileName = file.getOriginalFilename();
        //判断文件是否是excel文件
        if(!fileName.endsWith(xls) && !fileName.endsWith(xlsx)){
            throw new IOException(fileName + "不是excel文件");
        }
    }
    //获取工作簿
    private static Workbook getWorkBook(MultipartFile file) {
        //获得文件名
        String fileName = file.getOriginalFilename();
        //创建Workbook工作薄对象，表示整个excel
        Workbook workbook = null;
        try {
            //获取excel文件的io流
            InputStream is = file.getInputStream();
            //根据文件后缀名不同(xls和xlsx)获得不同的Workbook实现类对象
            if(fileName.endsWith(xls)){
                //2003
                workbook = new HSSFWorkbook(is);
            }else if(fileName.endsWith(xlsx)){
                //2007
                workbook = new XSSFWorkbook(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workbook;
    }
    //获取当前单元格的值
    private static String getCellValue(Cell cell){
        String cellValue = "";
        if(cell == null){
            return cellValue;
        }
        String dataFormatString = cell.getCellStyle().getDataFormatString();
        //如果当前单元格是空值，就返回""，低版本(3.x版本)获取类型是Cell.CELL_TYPE_BLANK
        if(cell.getCellType()== CellType.BLANK){
            return "";
        }

        if(dataFormatString.equals("m/d/yy")){
            cellValue = new SimpleDateFormat(DATE_FORMAT).format(cell.getDateCellValue());
            return cellValue;
        }
        //把数字当成String来读，避免出现1读成1.0的情况,低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_NUMERIC
        if(cell.getCellType() == CellType.NUMERIC){
            cell.setCellType(CellType.STRING); //低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_NUMERIC
        }
        //判断数据的类型
        switch (cell.getCellType()){
            case NUMERIC: //数字，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_NUMERIC
                cellValue = String.valueOf(cell.getNumericCellValue());
                System.out.println("cellValue = " + cellValue);
                break;
            case STRING: //字符串，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_NUMERIC
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case BOOLEAN: //Boolean，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_BOOLEAN
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA: //公式，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_FORMULA
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case BLANK: //空值，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_FORMULA
                cellValue = "";
                break;
            case ERROR: //故障，低版本(3.x版本)获取单元格类型是Cell.CELL_TYPE_ERROR
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }
}
