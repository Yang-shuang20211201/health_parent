package com.czjk.controller;

import com.czjk.constant.MessageConstant;
import com.czjk.entity.PageResult;
import com.czjk.entity.QueryPageBean;
import com.czjk.entity.Result;
import com.czjk.pojo.CheckGroup;
import com.czjk.pojo.Setmeal;
import com.czjk.service.SetMealService;
import com.czjk.util.AliOSSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 套餐管理
 */
@RestController
@RequestMapping("/setmeals")
public class SetMealController {

    @Autowired
    private SetMealService setMealService;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 分页查询套餐
     * @param queryPageBean 查询条件
     * @return
     */
    @GetMapping("/findByPage")
    public Result findByPage(QueryPageBean queryPageBean) {
        PageResult pageResult = setMealService.findByPage(queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,pageResult);
    }

    /**
     * 添加套餐
     * @param setmeal 套餐数据
     * @param ids 关联的检查组id
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Setmeal setmeal, Integer[] ids) {
        setMealService.add(setmeal, ids);
        return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);
    }

    /**
     * 上传套餐图片
     * @param imgFile
     * @return 图片在aliyun oss中的路径
     * @throws IOException
     */
    @PostMapping("/upload")
    public Result upload(@RequestParam("imgFile") MultipartFile imgFile) throws IOException {
        String imgUrl = aliOSSUtils.upload(imgFile);
        //将上传到aliyun oss中的图片封装到Result中返回
        return new Result(true,MessageConstant.UPLOAD_SUCCESS,imgUrl);
    }

    /**
     * 根据id查询套餐
     * @param id 套餐id
     * @return
     */
    @GetMapping("{id}")
    public Result findById(@PathVariable Integer id) {
        Setmeal setmeal = setMealService.findById(id);
        return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,setmeal);
    }

    /**
     * 根据套餐id查询关联的检查组
     * @param id 套餐id
     * @return
     */
    @GetMapping("/findCheckGroupById/{id}")
    public Result findCheckGroupById(@PathVariable Integer id) {
        List<CheckGroup> checkGroups = setMealService.findCheckGroupById(id);
        return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroups);
    }

    /**
     * 修改套餐
     * @param setmeal 套餐数据
     * @param ids 关联的检查组id
     * @return
     */
    @PutMapping
    public Result update(@RequestBody Setmeal setmeal, Integer[] ids) {
        setMealService.update(setmeal, ids);
        return new Result(true, MessageConstant.EDIT_SETMEAL_SUCCESS);
    }

    /**
     * 删除套餐
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        setMealService.delete(id);
        return new Result(true, MessageConstant.DELETE_SETMEAL_SUCCESS);
    }
}
