package com.czjk.controller;

import com.czjk.constant.MessageConstant;
import com.czjk.entity.PageResult;
import com.czjk.entity.QueryPageBean;
import com.czjk.entity.Result;
import com.czjk.pojo.CheckGroup;
import com.czjk.pojo.CheckItem;
import com.czjk.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 检查组管理
 */
@RestController
@RequestMapping("/checkGroups")
public class CheckGroupController {

    @Autowired
    private CheckGroupService checkGroupService;

    /**
     * 分页查询检查组
     * @param queryPageBean 查询条件
     * @return
     */
    @GetMapping("/findByPage")
    public Result findByPage(QueryPageBean queryPageBean){
        PageResult pageResult = checkGroupService.findByPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize(), queryPageBean.getQueryString());
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,pageResult);
    }

    /**
     * 根据检查组id获取检查项
     * @param id
     * @return
     */
    @GetMapping("/findCheckItemsById/{id}")
    public Result findCheckItemsById(@PathVariable Integer id){
        List<CheckItem> checkItems = checkGroupService.findCheckItemsById(id);
        return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItems);
    }

    /**
     * 根据检查组id获取检查组
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public Result findById(@PathVariable Integer id){
        CheckGroup checkGroup = checkGroupService.findById(id);
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroup);
    }


    /**
     * 添加检查组
     * @param checkGroup
     * @param ids
     * @return
     */
    @PostMapping
    public Result addCheckGroup(@RequestBody CheckGroup checkGroup, Integer[] ids){
        checkGroupService.addCheckGroup(checkGroup,ids);
        return new Result(true,MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }

    /**
     * 更新检查组
     * @param checkGroup
     * @param ids
     * @return
     */
    @PutMapping
    public Result updateCheckGroup(@RequestBody CheckGroup checkGroup, Integer[] ids){
        checkGroupService.updateCheckGroup(checkGroup,ids);
        return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS);
    }

    /**
     * 删除检查组
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public Result deleteCheckGroup(@PathVariable Integer id){
        checkGroupService.deleteCheckGroup(id);
        return new Result(true,MessageConstant.DELETE_CHECKGROUP_SUCCESS);
    }

    /**
     * 查找所有检查组
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
        List<CheckGroup> checkGroups = checkGroupService.findAll();
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroups);
    }
}
