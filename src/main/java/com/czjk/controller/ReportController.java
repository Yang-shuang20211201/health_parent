package com.czjk.controller;

import com.czjk.entity.Result;
import com.czjk.service.MemberService;
import com.czjk.service.OrderService;
import com.czjk.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 报表管理
 */
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private MemberService memberService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ReportService reportService;


    @RequestMapping("/getMemberReport.do")
    public Map getMemberReport() {
        return memberService.findByDate();
    }

    @RequestMapping("/getSetmealReport.do")
    public Map getSetmealReport() {
        return orderService.findBySetmeal();
    }


    @RequestMapping("/getBusinessReportData.do")
    public Map getBusinessReportData() throws Exception {
        Map map = reportService.getBusinessReportData();
        return map;
    }

    @RequestMapping("/exportBusinessReport.do")
    public Result exportBusinessReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return null;
    }
}
