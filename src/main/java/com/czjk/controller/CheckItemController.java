package com.czjk.controller;

import com.czjk.constant.MessageConstant;
import com.czjk.entity.PageResult;
import com.czjk.entity.QueryPageBean;
import com.czjk.entity.Result;
import com.czjk.pojo.CheckItem;
import com.czjk.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 检查项管理
 */
@RestController
@RequestMapping("/checkItems")
public class CheckItemController {

    @Autowired
    private CheckItemService checkItemService;

    /**
     * 分页查询检查项
     */
    @GetMapping("/findByPage")
    public Result findByPage(QueryPageBean queryPageBean) {
        PageResult pageResult = checkItemService.findByPage(queryPageBean.getCurrentPage(),
                                                            queryPageBean.getPageSize(),
                                                            queryPageBean.getQueryString());
        return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,pageResult);
    }

    /**
     * 添加检查项
     */
    @PostMapping
    public Result add(@RequestBody CheckItem checkItem) {
        checkItemService.add(checkItem);
        return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
    }

    /**
     * 根据id查询检查项
     */
    @GetMapping("{id}")
    public Result findById(@PathVariable Integer id) {
        CheckItem checkItem = checkItemService.findById(id);
        return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
    }

    /**
     * 修改检查项
     */
    @PutMapping
    public Result update(@RequestBody CheckItem checkItem) {
        checkItemService.update(checkItem);
        return new Result(true,MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }

    /**
     * 删除检查项
     */
    @DeleteMapping("{id}")
    public Result deleteCheckItem(@PathVariable Integer id) {
        checkItemService.deleteCheckItem(id);
        return new Result(true,MessageConstant.DELETE_CHECKITEM_SUCCESS);
    }

    //查询所有检查项
    @GetMapping("/findAll")
    public Result findAll() {
        List<CheckItem> checkItems = checkItemService.findAll();
        return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItems);
    }
}
