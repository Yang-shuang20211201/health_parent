package com.czjk.mapper;


import com.czjk.pojo.Setmeal;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 套餐Mapper
 */
@Mapper
public interface SetMealMapper  {

    /**
     * 查询套餐
     * @param queryString 查询条件
     * @return
     */
    List<Setmeal> findAll(String queryString);

    /**
     * 插入套餐数据
     * @param Setmeal
     */
    @Insert("insert into t_setmeal values(null,#{name},#{code},#{helpCode},#{sex},#{age},#{price},#{remark},#{attention},#{img})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void add(Setmeal Setmeal);

    /**
     * 获取套餐
     * @param id
     * @return
     */
    @Select("select * from t_setmeal where id=#{id}")
    Setmeal findById(Integer id);

    /**
     * 更新套餐信息
     * @param Setmeal
     */
    @Update("update t_setmeal set name=#{name},code=#{code},helpCode=#{helpCode},sex=#{sex},age=#{age},price=#{price},remark=#{remark},attention=#{attention},img=#{img} where id=#{id}")
    void update(Setmeal Setmeal);

    /**
     * 删除套餐信息
     * @param id
     */
    @Delete("delete from t_setmeal where id=#{id}")
    void delete(Integer id);
}
