package com.czjk.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 会员Mapper
 */
@Mapper
public interface MemberMapper  {


    @Select("SELECT MONTH(regTime) AS time,COUNT(*) total  FROM t_member WHERE YEAR(regTime) = YEAR(NOW()) GROUP BY MONTH(regTime)")
    List<Map> findByDate();
}
