package com.czjk.mapper;


import com.czjk.pojo.CheckGroup;
import com.czjk.pojo.CheckItem;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 *
 */
@Mapper
public interface CheckGroupMapper {
    /**
     * 查询检查组
     * @param queryString 查询条件
     * @return
     */
    List<CheckGroup> findAll(String queryString);

    /**
     * 插入检查组数据
     * @param checkGroup
     */
    @Insert("insert into t_checkgroup values(null,#{code},#{name},#{helpCode},#{sex},#{remark},#{attention})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void add(CheckGroup checkGroup);

    /**
     * 获取检查组
     * @param id
     * @return
     */
    @Select("select * from t_checkgroup where id=#{id}")
    CheckGroup findById(Integer id);

    /**
     * 更新检查组信息
     * @param checkGroup
     */
    @Update("update t_checkgroup set code=#{code},name=#{name},helpCode=#{helpCode},sex=#{sex},remark=#{remark},attention=#{attention} where id=#{id}")
    void update(CheckGroup checkGroup);

    /**
     * 删除检查组信息
     * @param id
     */
    @Delete("delete from t_checkgroup where id=#{id}")
    void delete(Integer id);

    /**
     * 根据套餐id查询检查组
     */
    @Select("select c1.* from t_checkgroup c1,t_setmeal_checkgroup c2 where c1.id=c2.checkgroup_id and c2.setmeal_id=#{setmealId}")
    List<CheckGroup> findCheckGroupsByCheckGroupId(Integer setmealId);
}
