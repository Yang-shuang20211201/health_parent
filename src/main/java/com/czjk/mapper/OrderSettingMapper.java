package com.czjk.mapper;

import com.czjk.pojo.OrderSetting;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Mapper
public interface OrderSettingMapper  {
    /**
     * 查询当前月份的预约数
     * @param currentYear 当前年份
     * @param currentMonth 当前月份
     * @return
     */
    @Select("select id,dayofmonth(orderDate) date,number,reservations from t_ordersetting where year(orderDate)=#{currentYear} and month(orderDate)=#{currentMonth}")
    List<Map> findAll(String currentYear, String currentMonth);

    /**
     * 修改可预约人数
     * @param orderSetting
     * @return 影响的行数，如果为0表示没有该日期的预约设置信息，后期添加预约设置信息
     */
    @Update("update t_ordersetting set number=#{number} where orderDate=#{orderDate}")
    int update(OrderSetting orderSetting);

    /**
     * 添加可预约人数
     * @param orderSetting
     */
    @Insert("insert into t_ordersetting value(null,#{orderDate},#{number},0)")
    void add(OrderSetting orderSetting);
}
