package com.czjk.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 */
@Mapper
public interface SetMealAndCheckgroupMapper  {
    /**
     * 添加套餐和检查组的关联关系
     * @param setmealId
     * @param checkGroupId
     */
    @Insert("insert into t_setmeal_checkgroup values(#{setmealId},#{checkGroupId})")
    void addSetmealAndCheckGroup(Integer setmealId, Integer checkGroupId);

    /**
     * 删除套餐和检查组的关联关系
     * @param setmealId
     */
    @Delete("delete from t_setmeal_checkgroup where setmeal_id=#{setmealId}")
    void deleteSetmealAndCheckGroup(Integer setmealId);
}
