package com.czjk.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 */
@Mapper
public interface CheckGroupAndCheckItemMapper {

    /**
     * 添加检查组和检查项的关联关系
     * @param checkGroupId
     * @param checkItemId
     */
    @Insert("insert into t_checkgroup_checkitem values(#{checkGroupId},#{checkItemId})")
    void addCheckGroupAndCheckItem(Integer checkGroupId, Integer checkItemId);

    /**
     * 删除检查组和检查项的关联关系
     * @param checkGroupId
     */
    @Delete("delete from t_checkgroup_checkitem where checkgroup_id=#{checkGroupId}")
    void deleteCheckGroupAndCheckItem(Integer checkGroupId);
}
