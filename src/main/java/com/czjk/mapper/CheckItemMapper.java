package com.czjk.mapper;


import com.czjk.pojo.CheckItem;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 *
 */
@Mapper
public interface CheckItemMapper {

    /**
     * 查询检查项
     * @param queryString 查询条件
     * @return
     */
    List<CheckItem> findAll(String queryString);

    /**
     * 添加检查项
     */
    @Insert("insert into t_checkitem values(null,#{code},#{name},#{sex},#{age},#{price},#{type},#{attention},#{remark})")
    void add(CheckItem checkItem);

    /**
     * 根据id查询对象
     */
    @Select("select * from t_checkitem where id=#{id}")
    CheckItem findById(Integer id);

    /**
     * 修改checkItem
     */
    @Update("update t_checkitem set code=#{code},name=#{name},sex=#{sex},age=#{age},price=#{price},type=#{type},attention=#{attention},remark=#{remark} where id=#{id}")
    void update(CheckItem checkItem);

    /**
     * 删除
     */
    @Delete("delete from t_checkitem where id=#{id}")
    void deleteCheckItem(Integer id);

    /**
     * 根据检查组id查询检查项
     */
    @Select("select c1.* from t_checkitem c1,t_checkgroup_checkitem c2 where c1.id=c2.checkitem_id and c2.checkgroup_id=#{checkGroupId}")
    List<CheckItem> findCheckItemsByCheckGroupId(Integer checkGroupId);
}
