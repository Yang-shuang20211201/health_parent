package com.czjk.init;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 *
 */
@Component
public class InitJob {

    //初始化容器时 执行该方法 即每隔一分钟 清理一下远程的服务器
//    @Scheduled(cron = "0/30 * * * * ?")
    public void cleanImg() {
        System.out.println("定时任务开始了");
    }
}
